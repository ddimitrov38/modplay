// by Dimitar Dimitrov

#include <stdlib.h>
#include <stdio.h>
#include <memory.h>

#ifdef OSX
    #include <OpenAL/al.h>
    #include <OpenAL/alc.h>
#else
    #include <AL/al.h>
    #include <AL/alc.h>
#endif

struct RIFF_HEADER {
    char chunkID[4];
    int chunkSize;
    char format[4];
};

struct WAVE_FORMAT {
    char subChunkID[4];
    int subChunkSize;
    short audioFormat;
    short numChannels;
    int sampleRate;
    int byteRate;
    short blockAlign;
    short bitsPerSample;
};

typedef struct {
    char subChunkID[4]; //should contain the word data
    int subChunk2Size; //Stores the size of the data block
} WAVE_DATA;

typedef struct {
    struct RIFF_HEADER riff;
    struct WAVE_FORMAT wave_format;
} WAVE_HEADER;


/*
 * These are 3D cartesian vector coordinates. A structure or class would be
 * a more flexible of handling these, but for the sake of simplicity we will
 * just leave it as is.
 */

// Position of the source sound.
ALfloat SourcePos[] = { 0.0, 0.0, 0.0 };

// Velocity of the source sound.
ALfloat SourceVel[] = { 0.0, 0.0, 0.0 };


// Position of the Listener.
ALfloat ListenerPos[] = { 0.0, 0.0, 0.0 };

// Velocity of the Listener.
ALfloat ListenerVel[] = { 0.0, 0.0, 0.0 };

// Orientation of the Listener. (first 3 elements are "at", second 3 are "up")
// Also note that these should be units of '1'.
ALfloat ListenerOri[] = { 0.0, 0.0, -1.0,  0.0, 1.0, 0.0 };


ALboolean LoadWAVFile(ALbyte *fname, ALenum *format, ALvoid **data, ALsizei *size, ALsizei *freq, ALboolean *loop) {
    FILE *f;
    WAVE_HEADER hdr;
    WAVE_DATA chunk;

    bzero(&hdr, sizeof(hdr));
    bzero(&chunk, sizeof(chunk));

    f = fopen(fname, "rb");

    if (!f)
        return AL_FALSE;

    fread(&hdr, 1, sizeof(hdr), f);

    while(memcmp(chunk.subChunkID,"data", 4) != 0 && !feof(f)) {
        fseek(f, chunk.subChunk2Size, SEEK_CUR);
        fread(&chunk, 1, sizeof(chunk), f);
    }

    *data = (ALvoid *) malloc(chunk.subChunk2Size);
    fread(*data, chunk.subChunk2Size, 1, f);

    *size = chunk.subChunk2Size;
    *freq = hdr.wave_format.sampleRate;

    if (hdr.wave_format.numChannels == 1) {
        if (hdr.wave_format.bitsPerSample == 8 )
            *format = AL_FORMAT_MONO8;
        else if (hdr.wave_format.bitsPerSample == 16)
            *format = AL_FORMAT_MONO16;
    } else if (hdr.wave_format.numChannels == 2) {
        if (hdr.wave_format.bitsPerSample == 8 )
            *format = AL_FORMAT_STEREO8;
        else if (hdr.wave_format.bitsPerSample == 16)
            *format = AL_FORMAT_STEREO16;
    }

    fclose(f);

    return AL_TRUE;
}


ALboolean LoadALData(const char *fname, ALuint *Buffer, ALuint *Source)
{
    // Variables to load into.
    ALenum format;
    ALsizei size;
    ALvoid* data;
    ALsizei freq;
    ALboolean loop;

    // Load wav data into a buffer.
    alGenBuffers(1, Buffer);

    if(alGetError() != AL_NO_ERROR)
        return AL_FALSE;

    if (!LoadWAVFile((ALbyte*) fname, &format, &data, &size, &freq, &loop))
        return AL_FALSE;

    alBufferData(*Buffer, format, data, size, freq);

    free(data);

    // Bind the buffer with the source.
    alGenSources(1, Source);

    if(alGetError() != AL_NO_ERROR)
        return AL_FALSE;

    alSourcei (*Source, AL_BUFFER,   *Buffer   );
    alSourcef (*Source, AL_PITCH,    1.0      );
    alSourcef (*Source, AL_GAIN,     1.0      );
    alSourcefv(*Source, AL_POSITION, SourcePos);
    alSourcefv(*Source, AL_VELOCITY, SourceVel);
    alSourcei (*Source, AL_LOOPING,  loop     );

    alGetError();
    // Do another error check and return.
    if(alGetError() == AL_NO_ERROR)
        return AL_TRUE;

    return AL_FALSE;
}


/*
 * void SetListenerValues()
 *
 *  We already defined certain values for the Listener, but we need
 *  to tell OpenAL to use that data. This function does just that.
 */
void SetListenerValues()
{
    alListenerfv(AL_POSITION,    ListenerPos);
    alListenerfv(AL_VELOCITY,    ListenerVel);
    alListenerfv(AL_ORIENTATION, ListenerOri);
}


int main(int ac, char *av[]) {
    ALuint buffer, source;
    ALint state;
    ALCdevice *device; 
    ALCcontext *context; 

    if (ac < 2) {
        printf("Ussage: %s <filename.wav>\n", av[0]);

        return -1;
    }

    device = alcOpenDevice(NULL);
    context = alcCreateContext(device, NULL);
    alcMakeContextCurrent(context);

    alGetError();

    // Load the wav data.
    if(LoadALData(av[1], &buffer, &source) == AL_FALSE) {
        printf("Error loading data!\n");
        return 0;
    }

    SetListenerValues();

    alSourcePlay(source);

    // Wait to finish
    alGetSourcei(source, AL_SOURCE_STATE, &state);
    while (state == AL_PLAYING) {
        alGetSourcei(source, AL_SOURCE_STATE, &state);
        sleep(1);
    }

    alDeleteSources(1, &source);
    alDeleteBuffers(1, &buffer);

    alcDestroyContext(context);
    alcCloseDevice(device);

    return 0;
}
