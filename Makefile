# by Dimitar Dimitrov

CC=gcc
CFLAGS=-Wall
LDFLAGS=`sdl-config --libs`

all: clean micromod-sdl mod

clean:
	rm -f micromod mod

micromod-sdl:
	$(CC) $(CFLAGS) micromod.c sdlplayer.c -o micromod $(LDFLAGS)

mod:
	$(CC) mod.c -o mod

