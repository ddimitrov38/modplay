/*
 * by Dimitar Dimitrov
 * mitko@insecurebg.org
 *
 * Sofia, Bulgaria 2012
 */

#include <stdio.h>
#include <memory.h>
#include <stdint.h>

#define MOD_NSAMPLES 31

typedef struct mod_sample_t {
	char name[22];
	uint16_t length;
	uint8_t finetune;
	uint8_t volume;
	uint16_t repeat_offset;
	uint16_t repeat_length;
} mod_sample;

typedef struct {
	char title[20];
	mod_sample sample[MOD_NSAMPLES];
	uint8_t npatterns;
	uint8_t jmp_end;
	uint8_t pattern[128];
	char tag[4];
	uint8_t npatterns_played;
	uint8_t speed;
} mod_header;

typedef struct {
	unsigned char a1, a2, a3, a4;
	unsigned char b1, b2, b3, b4;
	unsigned char c1, c2, c3, c4;
	unsigned char d1, d2, d3, d4;
} line;

int main(int ac, char **av)  {
	FILE *fd; 
	int i, j;
	mod_header hdr;
	void *samples[MOD_NSAMPLES];

	line pattern[64];

	if (ac < 2) {
		printf("Ussage: %s <filename>\n", av[0]);
		return -1;
	}

	if ( (fd = fopen(av[1], "rb") ) < 0 ) {
		printf("Cannot open file [%s]\n", av[1]);
		return -1;
	}

	bzero(&hdr, sizeof hdr);
	fread(&hdr, sizeof(hdr), 1, fd);

	printf("\nOpened file [%s] (%lu)\n ----------- \n", av[1], sizeof(hdr));
	printf("Name: %s\nPatterns: %d\nSpeed: %d\nTag: %s\n\nSamples\n ----------- \n", hdr.title, hdr.npatterns , hdr.speed, hdr.tag);

	for (i = 0; i < MOD_NSAMPLES; i++) {
		printf("Sample %02d: '%s', length: %db, volume: %d, repeat: %i\n", i, hdr.sample[i].name, ntohs(hdr.sample[i].length) * 2, hdr.sample[i].volume, ntohs(hdr.sample[i].repeat_length));
	}

	printf("\n\nPatterns table\n ----------- \n");
	fseek(fd, 1084, SEEK_SET); // Go to the patterns data

	for (i = 0; i < hdr.npatterns; i++) {
		printf("\nPattern [%d]\n ----------- \n", hdr.pattern[i]);

		fread(&pattern, sizeof(pattern), 1 ,fd);

		for (j = 0; j < 64; j++) {
			printf("%02x: %d\t%d\t%x(%x)\t|\t", j, pattern[j].a3 >> 4, pattern[j].a2, pattern[j].a3 & 0xf, pattern[j].a4);
			printf("%d\t%d\t%x(%x)\t|\t", pattern[j].b3 >> 4, pattern[j].b2, pattern[j].b3 & 0xf, pattern[j].b4);
			printf("%d\t%d\t%x(%x)\t|\t", pattern[j].c3 >> 4, pattern[j].c2, pattern[j].c3 & 0xf, pattern[j].c4);
			printf("%d\t%d\t%x(%x)\n", pattern[j].d3 >> 4, pattern[j].d2, pattern[j].d3 & 0xf, pattern[j].d4);
		}
	}

	

	printf("\n");

	fclose(fd);

	return 0;
}
